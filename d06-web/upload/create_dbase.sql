# phpMyAdmin SQL Dump
# version 2.5.6
# http://www.phpmyadmin.net
#
# �������: localhost
# ������ �����������: 20 ��� 2004, ���� 08:30 PM
# ������ ����������: 4.0.18
# ������ PHP: 4.3.5
# 
# ���� : `mathest`
# 

# --------------------------------------------------------

#
# ���� ������ ��� ��� ������ `activities`
#

CREATE TABLE `activities` (
  `id` int(11) NOT NULL auto_increment,
  `cat_id` int(11) default '1',
  `activity_name` varchar(255) default '-',
  `school_name` varchar(100) default '-',
  `teacher_name` varchar(100) default '-',
  `teacher_email` varchar(100) default '-',
  `password` varchar(10) default NULL,
  `approved` int(11) default '0',
  `date_uploaded` date default '0000-00-00',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=2 ;

#
# '�������� ��������� ��� ������ `activities`
#


# --------------------------------------------------------

#
# ���� ������ ��� ��� ������ `categories`
#

CREATE TABLE `categories` (
  `id` int(11) NOT NULL auto_increment,
  `class_name` char(2) default '�',
  `section_name` varchar(255) default '-',
  `directory_name` varchar(255) default '-',
  `section_icon` varchar(255) default '-',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM PACK_KEYS=0 AUTO_INCREMENT=12 ;

#
# '�������� ��������� ��� ������ `categories`
#

INSERT INTO `categories` (`id`, `class_name`, `section_name`, `directory_name`, `section_icon`) VALUES (1, '�', '������� ��� �������', 'arithmoi_prakseis', 'top_middle_1.png'),
(2, '�', '���������', 'metriseis', 'top_middle_3.png'),
(3, '�', '������� ��� ����������� ���������', 'syllogi', 'top_middle_2.png'),
(4, '�', '���������', 'gewmetria', 'top_middle_4.png'),
(5, '�', '������������ �����������', 'diairetotita', 'top_middle_5.png'),
(6, '��', '������� ��� �������', 'arithmoi_prakseis', 'top_middle_1.png'),
(7, '��', '���������', 'metriseis', 'top_middle_3.png'),
(8, '��', '������� ��� ����������� ���������', 'syllogi', 'top_middle_2.png'),
(9, '��', '���������', 'gewmetria', 'top_middle_4.png'),
(10, '��', '���������', 'exiswseis', 'top_middle_6.png'),
(11, '��', '����� - ���������', 'logoi_analogies', 'top_middle_5.png');
